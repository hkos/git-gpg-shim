# git GnuPG shim

This application aims to emulate GnuPG interface as expected by Git for signature generation and verification.

## Usage

Go to a git repository and set the `gpg.program` config variable:

    git config gpg.program /path/to/target/debug/git-gpg-shim

## Signature verification

Certificate backend need to be running prior to using this program. See https://gitlab.com/wiktor/minics for more details.

The environment variable must be set pointing to the cert store:

```sh
export CERT_STORE_ROOT=http://localhost:3000
```

Certificates will be downloaded automatically based on committer's e-mail addresses and key IDs.

Example output on the sequoia repository (`shim:` prefixed lines are printed by this tool):

```
$ git log --show-signature
commit 4b368a4e8d1ea1ec1af9abb095c5d7a55572c38d (HEAD -> main, tag: sq/v0.25.0, origin/main, origin/HEAD)
shim: Certificate : D2F2C5D45BE9FDE6A4EE0AAF31855247603831FD
shim: Signing key : D1FE45FB978F6B65C4C0B9AA686F55B4AB2B3386
shim: Signing date: 2021-03-05 15:10:12
shim: Identity    : Justus Winter (Code Signing Key) <justus@...>
shim: Signature verified.
Author: Justus Winter <justus@...>
Date:   Fri Mar 5 15:02:03 2021 +0100

    sq: Release 0.25.0.

commit 1a2ac93e094721b6fb21c337a5f6bb350cfbef93 (tag: openpgp/v1.1.0)
shim: Certificate : D2F2C5D45BE9FDE6A4EE0AAF31855247603831FD
shim: Signing key : D1FE45FB978F6B65C4C0B9AA686F55B4AB2B3386
shim: Signing date: 2021-03-05 14:28:55
shim: Identity    : Justus Winter (Code Signing Key) <justus@...>
shim: Signature verified.
Author: Justus Winter <justus@...>
Date:   Fri Mar 5 14:28:54 2021 +0100

    openpgp: Release 1.1.0.

commit ad026605585d0a2451578f755f21d5c27c3c1503
shim: Certificate : D2F2C5D45BE9FDE6A4EE0AAF31855247603831FD
shim: Signing key : D1FE45FB978F6B65C4C0B9AA686F55B4AB2B3386
shim: Signing date: 2021-03-05 14:28:46
shim: Identity    : Justus Winter (Code Signing Key) <justus@...>
shim: Signature verified.
Author: Justus Winter <justus@...>
Date:   Fri Mar 5 14:25:42 2021 +0100

    openpgp: Do not recommend padding by default.

      - We discovered compatibility problems with the padding mechanism,
        so we should caution against its use when compatibility with
        certain implementations is required.  Also, don't use padding in
        the module's example.

commit 39539aa6105bc5e2173d211108be665495124aed (origin/wiktor-k/issue-682-verifier-bug)
shim: Certificate : 653909A2F0E37C106F5FAF546C8857E0D8E8F074
shim: Signing key : E7E2B84A36457BEA3F43692DE68BE3B312FA33FC
shim: Signing date: 2021-03-05 13:50:53
shim: Identity    : Wiktor Kwapisiewicz <wiktor@...>
shim: Signature verified.
Author: Wiktor Kwapisiewicz <wiktor@...>
Date:   Fri Feb 26 10:31:13 2021 +0100

    openpgp: Fix panic when verifying signatures.
```

## Signing commits/tags

For signing the previous step must be set up and an additional agent must be present.
Currently tested is the `pks-openpgp-card` store that uses OpenPGP cards and the `pks-agent` proxy that remembers PINs.

The environment variable for the agent must be set:

```sh
export PKS_AGENT_SOCK=/run/user/1000/pks-agent.sock
```

And then the signing operations in git will automatically work.

Note that the keys must be unlocked as git doesn't have the UI to ask the user for the PIN (that's why the agent is used).

This operation uses the `user.signingkey` variable that is passed automatically by git: it's either set explicitly by the user or it is user's e-mail.
The cert store must have this key and the first signing key that is found will be used.

## Uninstalling

Afterwards use `git config --unset gpg.program` to uninstall the shim.
