use std::{env::args, io::BufRead};

use openpgp::{
    cert::{amalgamation::ValidAmalgamation, CertParser},
    packet::signature::SignatureBuilder,
    serialize::stream::{Message, Signer},
    types::SignatureType,
};
use openpgp::{
    parse::{
        stream::{DetachedVerifierBuilder, MessageLayer, MessageStructure, VerificationHelper},
        Parse,
    },
    policy::StandardPolicy,
    serialize::stream::Armorer,
    Cert, KeyHandle,
};
use sequoia_openpgp as openpgp;

fn main() -> std::result::Result<(), Box<dyn std::error::Error>> {
    let args: Vec<_> = args().collect();

    if args[3] == "--verify" {
        let p = &StandardPolicy::new();
        let mut committer: Option<&str> = None;

        let mut data = vec![];
        std::io::copy(&mut std::io::stdin(), &mut data)?;

        let mut pointer = &data[..];
        let mut buf = String::new();
        while let Ok(size) = pointer.read_line(&mut buf) {
            if size == 0 || buf.is_empty() {
                break;
            }
            if buf.starts_with("committer ") {
                committer = Some(&buf[(buf.find('<').unwrap() + 1)..buf.find('>').unwrap()]);
                break;
            }
            buf.clear();
        }

        let h = Helper { committer };

        let mut v = DetachedVerifierBuilder::from_file(&args[4])?.with_policy(p, None, h)?;
        v.verify_bytes(&data)?;

        // git expects that: https://github.com/git/git/blob/225365fb5195e804274ab569ac3cc4919451dc7f/gpg-interface.c#L303
        println!("\n[GNUPG:] GOODSIG ");

        eprintln!("shim: Signature verified.")
    } else if args[2] == "-bsau" {
        let mut email: &str = &args[3];
        if email.contains('<') {
            email = &email[(email.find('<').unwrap() + 1)..email.find('>').unwrap()];
        }
        let client = reqwest::blocking::Client::new();
        let mut signing_key = None;

        let policy = &openpgp::policy::StandardPolicy::new();
        for cert in CertParser::from_bytes(
            &client
                .get(format!(
                    "{}/{}",
                    std::env::var("CERT_STORE_ROOT").expect("CERT_STORE_ROOT is set"),
                    email
                ))
                .send()?
                .bytes()?,
        )?
        .flatten()
        {
            if let Some(key) = cert
                .keys()
                .with_policy(policy, None)
                .supported()
                .alive()
                .revoked(false)
                .for_signing()
                .next()
            {
                signing_key = Some(key.key().clone());
                break;
            }
        }
        let signing_keypair = git_gpg_shim::unlock_signer(
            std::env::var("PKS_AGENT_SOCK").expect("PKS_AGENT_SOCK is set"),
            signing_key.unwrap(),
            &String::new().into(),
        )
        .unwrap();
        let mut sink = std::io::stdout();
        let message = Message::new(&mut sink);
        let message = Armorer::new(message)
            .kind(openpgp::armor::Kind::Signature)
            .build()?;
        let mut signer = Signer::with_template(
            message,
            signing_keypair,
            SignatureBuilder::new(SignatureType::Binary).set_signers_user_id(&email)?,
        )
        .detached()
        .build()?;

        std::io::copy(&mut std::io::stdin(), &mut signer)?;
        signer.finalize()?;

        // git expects that: https://github.com/git/git/blob/225365fb5195e804274ab569ac3cc4919451dc7f/gpg-interface.c#L467
        eprintln!("\n[GNUPG:] SIG_CREATED ");
    } else {
        println!("Hello, world!: {:?}", args);
    }

    Ok(())
}
struct Helper<'a> {
    committer: Option<&'a str>,
}

impl<'a> VerificationHelper for Helper<'a> {
    fn get_certs(&mut self, ids: &[KeyHandle]) -> openpgp::Result<Vec<Cert>> {
        let client = reqwest::blocking::Client::new();

        let mut certs = vec![];

        if let Some(committer) = self.committer {
            certs.extend(
                CertParser::from_bytes(
                    &client
                        .get(format!(
                            "{}/{}",
                            std::env::var("CERT_STORE_ROOT").expect("CERT_STORE_ROOT is set"),
                            committer
                        ))
                        .send()?
                        .bytes()?,
                )?
                .filter_map(|result| result.ok()),
            );
        }

        for id in ids {
            certs.extend(
                CertParser::from_bytes(
                    &client
                        .get(format!(
                            "{}/{:X}",
                            std::env::var("CERT_STORE_ROOT").expect("CERT_STORE_ROOT is set"),
                            id
                        ))
                        .send()?
                        .bytes()?,
                )?
                .filter_map(|result| result.ok()),
            );
        }

        Ok(certs)
    }

    fn check(&mut self, structure: MessageStructure) -> openpgp::Result<()> {
        for layer in structure.into_iter() {
            match layer {
                MessageLayer::SignatureGroup { ref results } => {
                    if !results.iter().any(|r| r.is_ok()) {
                        return Err(anyhow::anyhow!("No valid signature: {:?}", results[0]));
                    }
                    for gc in results.iter().flatten() {
                        eprintln!("shim: Certificate : {:X}", gc.ka.cert().fingerprint());
                        eprintln!("shim: Signing key : {:X}", gc.ka.fingerprint());
                        if let Some(date) = gc.sig.signature_creation_time() {
                            use chrono::prelude::{DateTime, Local};
                            let dt: DateTime<Local> = date.into();
                            eprintln!("shim: Signing date: {}", dt.format("%Y-%m-%d %H:%M:%S"));
                        }
                        let cert = gc
                            .ka
                            .cert()
                            .clone()
                            .with_policy(gc.ka.policy(), None)?
                            .primary_userid()?;
                        let user_id = String::from_utf8_lossy(cert.value());
                        eprintln!("shim: Identity    : {}", user_id);
                    }
                }
                _ => return Err(anyhow::anyhow!("Unexpected message structure")),
            }
        }
        Ok(())
    }
}
